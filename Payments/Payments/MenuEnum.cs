﻿namespace Payments
{
    /// <summary>
    /// Перечисление пунктов меню.
    /// </summary>
    public enum  MenuEnum
    {
        /// <summary>
        /// Вывод на экран.
        /// </summary>
        Output = 1,

        /// <summary>
        /// Сортировка.
        /// </summary>
        Sorting = 2,

        /// <summary>
        /// Добавление в список.
        /// </summary>
        Add = 3,

        /// <summary>
        /// Нахождение суммы.
        /// </summary>
        Sum = 4,

        /// <summary>
        /// Выбор другого типа платежей.
        /// </summary>
        ExitPayment = 5,

        /// <summary>
        /// Выход из программы.
        /// </summary>
        Exit = 6
    }
}
