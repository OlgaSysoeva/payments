﻿namespace Payments
{
    /// <summary>
    /// Интерфейс стратегии.
    /// </summary>
    public interface IStrategy
    {
        /// <summary>
        /// Запуск метода стратегии.
        /// </summary>
        /// <param name="paymentsList">Класс списка платежей с типом BaseList.</param>
        void DoAlgoritm(BaseList paymentsList);
    }
}

// Принцип разделения интерфейса. Каждый интерфейс отвечает только за определенный функционал, 
//  используемый классами.