﻿namespace Payments
{
    /// <summary>
    /// Контекст для выбора стратегии.
    /// </summary>
    public interface IContext
    {
        /// <summary>
        /// Устанавливает вид стратегии.
        /// </summary>
        /// <param name="strategy">Стратегия с типом IStrategy.</param>
        void SetStrategy(IStrategy strategy);

        /// <summary>
        /// Запускает выплнение стратегии.
        /// </summary>
        /// <param name="paymentsList">Список платежей типа BaseList.</param>
        void DoAlgoritm(BaseList paymentsList);
    }
}

// Принцип открытости/закрытости. Применен паттерн "Стратегия", который позволяет добавить операции
// и таким образом расширять без изменений существующего кода.

// Принцип разделения интерфейса. Каждый интерфейс отвечает только за определенный функционал, 
//  используемый классами.
