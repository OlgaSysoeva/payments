﻿using System;

namespace Payments
{
    class Program
    {
        static BaseList _payments = new BaseList();
        static readonly BaseList _cashPaymentList = new CashPaymentList();
        static readonly BaseList _cashlessPaymentList = new CashlessPaymentList();
        static readonly IContext _context = new Context();
        const string InvalidValueMessage = "Недопустимое значение.";

        static void Main()
        {
            MenuStrategy();
        }

        /// <summary>
        /// Меню управления платежами.
        /// </summary>
        static void MenuStrategy()
        {
            MenuPayment();

            while (true)
            {
                Console.WriteLine("\nВыберите пункт меню:");
                Console.WriteLine("1 Отобразить платежи.");
                Console.WriteLine("2 Отсортировать платежи по возрастанию даты оплаты.");
                Console.WriteLine("3 Добавить сгенерированный платеж.");
                Console.WriteLine("4 Отобразить общую сумму.");
                Console.WriteLine("5 Выбор другого типа платежа.");
                Console.WriteLine("6 Выйти из программы.");
                Console.Write("Введите номер пункта: ");

                if (int.TryParse(Console.ReadLine(), out int value))
                {
                    Console.WriteLine();
                    switch (value)
                    {
                        case (int)MenuEnum.Add:
                            _context.SetStrategy(new Add());
                            _context.DoAlgoritm(_payments);
                            _context.SetStrategy(new Output());
                            break;
                        case (int)MenuEnum.Sorting:
                            _context.SetStrategy(new Sorter());
                            _context.DoAlgoritm(_payments);
                            _context.SetStrategy(new Output());
                            break;
                        case (int)MenuEnum.Output:
                            _context.SetStrategy(new Output());
                            break;
                        case (int)MenuEnum.Sum:
                            _context.SetStrategy(new Calculator());
                            break;
                        case (int)MenuEnum.ExitPayment:
                            MenuPayment();
                            continue;
                        case (int)MenuEnum.Exit:
                            return;
                        default:
                            Console.WriteLine(InvalidValueMessage);
                            continue;
                    };

                    _context.DoAlgoritm(_payments);
                }
                else
                {
                    Console.WriteLine(InvalidValueMessage);
                }
            }
        }

        /// <summary>
        /// Меню управления типами платежей.
        /// </summary>
        static void MenuPayment()
        {
            while (true)
            {
                Console.WriteLine("Выберите тип платежа:");
                Console.WriteLine("1 Платежи с наличным расчетом.");
                Console.WriteLine("2 Платежи с безналичным расчетом.");
                Console.Write("Введите номер пункта: ");

                if (int.TryParse(Console.ReadLine(), out int value))
                {
                    switch (value)
                    {
                        case (int)PaymentTypeEnum.CashPayment:
                            _payments = _cashPaymentList;
                            return;
                        case (int)PaymentTypeEnum.CashlessPayment:
                            _payments = _cashlessPaymentList;
                            return;
                        default:
                            Console.WriteLine(InvalidValueMessage);
                            continue;
                    };
                }
                else
                {
                    Console.WriteLine(InvalidValueMessage);
                }
            }
        }
    }
}

// Принцип единственной ответственности. Класс отвечает только за один функционал - работа с меню.

// Принцип открытости/закрытости. Применен паттерн "Стратегия", который позволяет добавить операции
// и таким образом расширять без изменений существующего кода.

// Принцип подстановки Лисков. Используется для классов PaymentList. Создается объект базового класса 
// BaseList, но используется дочерний класс CashPaymentList. Также применяется в методе контекста SetOperation.

// Принцип разделения интерфейса. Каждый интерфейс отвечает только за определенный функционал, 
//  используемый классами.

// Принцип инверсии зависимостей. Чтобы не привязывать к реализации используются интерфейсы IContext и 
// IOperation.
