﻿namespace Payments
{
    /// <summary>
    /// Класс реализации контекста стратегии.
    /// </summary>
    public class Context : IContext
    {

        IStrategy _strategy;

        public void SetStrategy(IStrategy strategy)
        {
            _strategy = strategy;
        }

        public void DoAlgoritm(BaseList paymentsList)
        {
            _strategy.DoAlgoritm(paymentsList);
        }
    }
}
