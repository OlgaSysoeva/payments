﻿namespace Payments
{
    /// <summary>
    /// Перечисление типов платежей.
    /// </summary>
    public enum PaymentTypeEnum
    {
        /// <summary>
        /// Наличные платежи.
        /// </summary>
        CashPayment = 1,

        /// <summary>
        /// Бзналичные платежи.
        /// </summary>
        CashlessPayment = 2
    }
}
