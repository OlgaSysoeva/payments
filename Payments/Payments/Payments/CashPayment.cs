﻿using Bogus;

using System;

namespace Payments
{
    /// <summary>
    /// Информация о наличном платеже.
    /// </summary>
    public class CashPayment : BasePayment
    {
        /// <summary>
        /// Номер кассы.
        /// </summary>
        public int CashierNumber { get; set; }

        /// <summary>
        /// Генерирует новый платеж типа CashPayment.
        /// </summary>
        /// <returns>Возвращает новый платеж.</returns>
        public CashPayment NewCashPayment()
        {
            var customersFaker = CreateFaker();

            var newPayment = customersFaker.Generate();

            return newPayment;
        }

        /// <summary>
        /// Настройка параметров генерации данных.
        /// </summary>
        /// <returns>Модель с настройками генерации.</returns>
        private Faker<CashPayment> CreateFaker()
        {
            var paymentFaker = new Faker<CashPayment>()
                .RuleFor(u => u.Service, (f, u) => f.Company.CompanyName())
                .RuleFor(u => u.PaymentDate, (f, u) => f.Date.Future())
                .RuleFor(u => u.Amount, (f, u) => f.Finance.Amount())
                .RuleFor(u => u.Id, (f, u) => new Random().Next(1, 200))
                .RuleFor(u => u.CashierNumber, (f, u) => new Random().Next(1, 20));

            return paymentFaker;
        }
    }
}
