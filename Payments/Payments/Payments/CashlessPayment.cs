﻿using Bogus;

using System;

namespace Payments
{
    /// <summary>
    /// Информация о безналичном платеже.
    /// </summary>
    public class CashlessPayment  : BasePayment
    {
        /// <summary>
        /// Номер карты.
        /// </summary>
        public int CardNumber { get; set; }

        /// <summary>
        /// Генерирует новый платеж типа CashlessPayment.
        /// </summary>
        /// <returns>Возвращает новый платеж.</returns>
        public CashlessPayment NewCashlessPayment()
        {
            var customersFaker = CreateFaker();

            var newPayment = customersFaker.Generate();

            return newPayment;
        }

        /// <summary>
        /// Настройка параметров генерации данных.
        /// </summary>
        /// <returns>Модель с настройками генерации.</returns>
        private Faker<CashlessPayment> CreateFaker()
        {
            var paymentFaker = new Faker<CashlessPayment>()
                .RuleFor(u => u.Service, (f, u) => f.Company.CompanyName())
                .RuleFor(u => u.PaymentDate, (f, u) => f.Date.Future())
                .RuleFor(u => u.Amount, (f, u) => f.Finance.Amount())
                .RuleFor(u => u.Id, (f, u) => new Random().Next(1, 200))
                .RuleFor(u => u.CardNumber, (f, u) => new Random().Next(1111, 9999));

            return paymentFaker;
        }
    }
}
