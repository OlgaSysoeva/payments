﻿using Bogus;

using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Payments
{
    /// <summary>
    /// Базовый класс платежей.
    /// </summary>
    public class BasePayment
    {
        /// <summary>
        /// Идентификатор платежа.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата оплаты.
        /// </summary>
        public DateTime PaymentDate { get; set; }

        /// <summary>
        /// Сервис/услуга оплаты.
        /// </summary>
        public string Service { get; set; }

        /// <summary>
        /// Сумма оплаты.
        /// </summary>
        public decimal Amount { get; set; }

        public override string ToString()
        {
            var props = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            var strData = new StringBuilder();

            var propId = props.Where(x => x.Name == nameof(Id)).Single();
            strData.Append($"{propId.Name} : {propId.GetValue(this)} ");

            foreach (var prop in props.Where(x => x.Name != nameof(Id)))
            {
                strData.Append($"{prop.Name} : {prop.GetValue(this)} ");
            }

            return strData.ToString();
        }

        /// <summary>
        /// Генерирует новый платеж типа BasePayment.
        /// </summary>
        /// <returns>Возвращает новый платеж.</returns>
        public BasePayment NewBasePayment()
        {
            var customersFaker = CreateFaker();

            var newPayment = customersFaker.Generate();
            
            return newPayment;
        }

        /// <summary>
        /// Настройка параметров генерации данных.
        /// </summary>
        /// <returns>Модель с настройками генерации.</returns>
        private Faker<BasePayment> CreateFaker()
        {
            var paymentFaker = new Faker<BasePayment>()
                .RuleFor(u => u.Service, (f, u) => f.Company.CompanyName())
                .RuleFor(u => u.PaymentDate, (f, u) => f.Date.Future())
                .RuleFor(u => u.Amount, (f, u) => f.Finance.Amount())
                .RuleFor(u => u.Id, (f, u) => new Random().Next(1, 200));

            return paymentFaker;
        }
    }
}

// Принцип открытости/закрытости.
// Используется базовый класс, что позволяет расширять и добавлять новые классы - разновидности платежей.
// В проекте используется 2 вида платежей: безналичные и наличные платежи.
