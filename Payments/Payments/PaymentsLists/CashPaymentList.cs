﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Payments
{
    /// <summary>
    /// Класс списка платежей.
    /// </summary>
    public class CashPaymentList : BaseList
    {
        public CashPaymentList()
        {
            CashPayments = new List<CashPayment>()
            {
                new CashPayment() {
                    Id = 1,
                    PaymentDate = new DateTime(2020, 10, 19),
                    Service = "Оплата интернет-заказа",
                    Amount = 25,
                    CashierNumber = 5
                },
                new CashPayment() {
                    Id = 2,
                    PaymentDate = new DateTime(2020, 10, 18),
                    Service = "Оплата стройматериалов",
                    Amount = 32,
                    CashierNumber = 7
                },
                new CashPayment() {
                    Id = 3,
                    PaymentDate = new DateTime(2020, 10, 20),
                    Service = "Оплата продуктов",
                    Amount = 48,
                    CashierNumber = 4
                },
                new CashPayment() {
                    Id = 4,
                    PaymentDate = new DateTime(2020, 10, 21),
                    Service = "Приобретение сертификата",
                    Amount = 15,
                    CashierNumber = 1
                }
            };
        }

        /// <summary>
        /// Список платежей типа CashPayment.
        /// </summary>
        public List<CashPayment> CashPayments { get; set; }

        public override void Add()
        {
            var newPayment = new CashPayment();

            do
            {
                newPayment = newPayment.NewCashPayment();
            }
            while (Exists(newPayment.Id) || newPayment.Id == 0);

            CashPayments.Add(newPayment);
        }

        public override bool Exists(int id)
        {
            return CashPayments.Where(x => x.Id == id).Any();
        }

        public override decimal GetSum()
        {
            return CashPayments.Sum(x => x.Amount);
        }

        public override void Output()
        {
            foreach (var item in CashPayments)
            {
                Console.WriteLine(item.ToString());
            }
        }

        public override void Sort()
        {
            CashPayments = CashPayments.OrderBy(x => x.PaymentDate).ToList();
        }
    }
}
