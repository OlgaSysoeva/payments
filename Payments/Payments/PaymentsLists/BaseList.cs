﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Payments
{
    /// <summary>
    /// Базовый список платежей.
    /// </summary>
    public class BaseList
    {
        /// <summary>
        /// Список платежей.
        /// </summary>
        public List<BasePayment> Payments { get; set; }

        /// <summary>
        /// Добавляет новый платеж в список.
        /// </summary>
        public virtual void Add()
        {
            var newPayment = new BasePayment();

            do
            {
                newPayment = newPayment.NewBasePayment();
            }
            while (Exists(newPayment.Id) || newPayment.Id == 0);

            Payments.Add(newPayment);
        }

        /// <summary>
        /// Проверяет на наличие платежа с заданным идентификатором.
        /// </summary>
        /// <param name="id">Идентификатор платежа.</param>
        /// <returns>Возвращает true если платеж есть в списке.</returns>
        public virtual bool Exists(int id)
        {
            return Payments.Where(x => x.Id == id).Any();
        }

        /// <summary>
        /// Получает сумму по всем платежам.
        /// </summary>
        /// <returns>Возвращает сумму по платежам.</returns>
        public virtual decimal GetSum()
        {
            return Payments.Sum(x => x.Amount);
        }

        /// <summary>
        /// Выводит платежи на консоль.
        /// </summary>
        public virtual void Output()
        {
            foreach (var item in Payments)
            {
                Console.WriteLine(item.ToString());
            }
        }

        /// <summary>
        /// Сортирует платежи по дате добавления.
        /// </summary>
        public virtual void Sort()
        {
            Payments = Payments.OrderBy(x => x.PaymentDate).ToList();
        }
    }
}

// Принцип открытости/закрытости.
// Используется базовый класс, что позволяет расширять и добавлять новые классы - разновидности платежей.
// В проекте используется 2 вида платежей: безналичные и наличные платежи.