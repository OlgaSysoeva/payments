﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Payments
{
    public class CashlessPaymentList : BaseList
    {
        public CashlessPaymentList()
        {
            CashlessPayments = new List<CashlessPayment>()
            {
                new CashlessPayment() {
                    Id = 1,
                    PaymentDate = new DateTime(2020, 10, 19),
                    Service = "Оплата интернет-заказа",
                    Amount = 25,
                    CardNumber = 2321
                },
                new CashlessPayment() {
                    Id = 2,
                    PaymentDate = new DateTime(2020, 10, 18),
                    Service = "Оплата стройматериалов",
                    Amount = 32,
                    CardNumber = 7865
                },
                new CashlessPayment() {
                    Id = 3,
                    PaymentDate = new DateTime(2020, 10, 20),
                    Service = "Оплата продуктов",
                    Amount = 48,
                    CardNumber = 9845
                },
                new CashlessPayment() {
                    Id = 4,
                    PaymentDate = new DateTime(2020, 10, 21),
                    Service = "Приобретение сертификата",
                    Amount = 15,
                    CardNumber = 3496
                }
            };
        }

        /// <summary>
        /// Список платежей типа CashlessPayment.
        /// </summary>
        public List<CashlessPayment> CashlessPayments { get; set; }

        public override void Add()
        {
            var newPayment = new CashlessPayment();

            do
            {
                newPayment = newPayment.NewCashlessPayment();
            }
            while (Exists(newPayment.Id) || newPayment.Id == 0);

            CashlessPayments.Add(newPayment);
        }

        public override bool Exists(int id)
        {
            return CashlessPayments.Where(x => x.Id == id).Any();
        }

        public override decimal GetSum()
        {
            return CashlessPayments.Sum(x => x.Amount);
        }

        public override void Output()
        {
            foreach (var item in CashlessPayments)
            {
                Console.WriteLine(item.ToString());
            }
        }

        public override void Sort()
        {
            CashlessPayments = CashlessPayments.OrderBy(x => x.PaymentDate).ToList();
        }
    }
}
