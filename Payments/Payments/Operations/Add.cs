﻿using System;

namespace Payments
{
    /// <summary>
    /// Добавляет платежи в список.
    /// </summary>
    class Add : IStrategy
    {
        public void DoAlgoritm(BaseList paymentsList)
        {
            if (paymentsList == null)
            {
                throw new ArgumentNullException(nameof(paymentsList));
            }

            paymentsList.Add();
        }
    }
}

// Принцип единственной ответственности. Класс отвечает только за один функционал - добавление данных.
