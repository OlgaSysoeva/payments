﻿using System;

namespace Payments
{
    /// <summary>
    /// Производит расчеты над платежами.
    /// </summary>
    public class Calculator : IStrategy
    {
        public void DoAlgoritm(BaseList paymentsList)
        {
            if (paymentsList == null)
            {
                throw new ArgumentNullException(nameof(paymentsList));
            }

            Console.WriteLine($"Сумма по платежам: {paymentsList.GetSum()}");
        }
    }
}

// Принцип единственной ответственности. Класс отвечает только за один функционал - расчет суммы.
