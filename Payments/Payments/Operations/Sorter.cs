﻿using System;

namespace Payments
{
    /// <summary>
    /// Сортирует платежи.
    /// </summary>
    public class Sorter : IStrategy
    {
        public void DoAlgoritm(BaseList paymentsList)
        {
            if (paymentsList == null)
            {
                throw new ArgumentNullException(nameof(paymentsList));
            }

            paymentsList.Sort();
        }
    }
}

// Принцип единственной ответственности. Класс отвечает только за один функционал - сортировку данных.
