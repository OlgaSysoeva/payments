﻿using System;

namespace Payments
{
    /// <summary>
    /// Вывод платежей.
    /// </summary>
    public class Output : IStrategy
    {
        public void DoAlgoritm(BaseList paymentsList)
        {
            if (paymentsList == null)
            {
                throw new ArgumentNullException(nameof(paymentsList));
            }

            paymentsList.Output();
        }
    }
}

// Принцип единственной ответственности. Класс отвечает только за один функционал - вывод информации.
